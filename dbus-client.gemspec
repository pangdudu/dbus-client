Gem::Specification.new do |s|
  s.name = %q{dbus-client}
  s.version = "0.0.1"
  s.specification_version = 2 if s.respond_to? :specification_version=
  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["pangdudu"]
  s.date = %q{2009-11-04}
  s.description = %q{a small dbus client lib written in ruby}
  s.email = %q{pangdudu@github}
  s.extra_rdoc_files = ["README.rdoc"]
  s.files = ["README.rdoc", "lib/dbus_client.rb"]
  s.has_rdoc = true
  s.homepage = %q{http://github.com/pangdudu/dbus-client}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.1}
  s.summary = %q{small dbus client lib}
  s.add_dependency(%q<pangdudu-ruby-dbus>, [">= 0"])
  s.add_dependency(%q<pangdudu-rofl>, [">= 0"])
end
